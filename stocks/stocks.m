clear;
% http://www.sciencedirect.com/science/article/pii/016541019290023U
% http://www.sciencedirect.com/science/article/pii/016541019090008R
% http://en.wikipedia.org/wiki/Historical_components_of_the_Dow_Jones_Industrial_Average
% http://finance.yahoo.com/q/cp?s=%5EDJI+Components

[Symbols,Starts] = stocks_info();

symbol = Symbols{10};
start  = Starts{10};

S1 = stocks_prep(symbol);
S2 = stocks_prep('DJI');

dno = datenum(start);
dnf = datenum([2015,4,1]);

I2 = find(S2(:,1) == dno);
t2x = S2(I2:end,1);
t2x = t2x-t2x(1)+1;
S2x = S2(I2:end,5);
tg = [1:dnf-dno+1];
S2 = NaN*zeros(length(tg),1);
S2(t2x) = S2x;

I1 = find(S1(:,1) == dno);
t1x = S1(I1:end,1);
t1x = t1x-t1x(1)+1;
S1x = S1(I1:end,7);
S1 = NaN*zeros(length(tg),1);
S1(t1x) = S1x;

if (0)
for i = 1:min(length(S1),length(S2))-1
    fprintf('%d %s %s %8.2f %8.2f\n',tg(i) - tg(i),datestr(dno+tg(i)-1),datestr(dno+tg(i)-1),S2(i),S1(i));
end
end

T = 5;

for i = 1:min(length(S1),length(S2))-1
    if (isnan(S1(i)) | isnan(S1(i+1)) | isnan(S2(i)) | isnan(S2(i+1)))
        continue;
    end
    dS1(i+1) = 100*(S1(i+1)-S1(i))/S1(i);
    dS2(i+1) = 100*(S2(i+1)-S2(i))/S2(i);
    dS(i+1)  = dS2(i+1)-dS1(i+1);
    %fprintf('%d %s %s %8.2f %8.2f\n',t2(i) - t1(i),datestr(t2(i)),datestr(t1(i)),S2(i),S1(i));
    %fprintf('%d %s %s %8.2f %8.2f\n',tg(i) - tg(i),datestr(dno+tg(i)-1),datestr(dno+tg(i)-1),S2(i),S1(i));
    if (1)
    if (dS(i+1) < -T)
        fprintf('- %s %8.2f %8.2f %+8.2f %8.1f %8.1f %+8.2f %+8.2f\n',datestr(dno+tg(i)-1),S1(i),S1(i+1),dS1(i+1),S2(i),S2(i+1),dS2(i+1),dS(i+1));
    else
        fprintf('  %s %8.2f %8.2f %+8.2f %8.1f %8.1f %+8.2f %+8.2f\n',datestr(dno+tg(i)-1),S1(i),S1(i+1),dS1(i+1),S2(i),S2(i+1),dS2(i+1),dS(i+1));
    end
    end
    if (1)
    if (dS(i+1) > T)
        fprintf('+ %s %8.2f %8.2f %+8.2f %8.1f %8.1f %+8.2f %+8.2f\n',datestr(dno+tg(i)-1),S1(i),S1(i+1),dS1(i+1),S2(i),S2(i+1),dS2(i+1),dS(i+1));
    else
        fprintf('  %s %8.2f %8.2f %+8.2f %8.1f %8.1f %+8.2f %+8.2f\n',datestr(dno+tg(i)-1),S1(i),S1(i+1),dS1(i+1),S2(i),S2(i+1),dS2(i+1),dS(i+1));
    end    
    end
end

Ip2 = find(dS2>T);
In2 = find(dS2<-T);
Ip1 = find(dS1>T);
In1 = find(dS1<-T);
Ip = find(dS>T);
In = find(dS<-T);

if Ip2(end) + 1 > length(tg),Ip2 = Ip2(1:end-1);end
if In2(end) + 1 > length(tg),In2 = In2(1:end-1);end
if Ip1(end) + 1 > length(tg),Ip1 = Ip1(1:end-1);end
if In1(end) + 1 > length(tg),In1 = In1(1:end-1);end
if Ip(end) + 1 > length(tg),Ip = Ip(1:end-1);end
if In(end) + 1 > length(tg),In = In(1:end-1);end

zp2 = dS2(Ip+1);
zp1 = dS1(Ip+1);
zp  = dS(Ip+1);

tp2 = dno+tg(Ip+1)-1;
tp1 = dno+tg(Ip+1)-1;
tp  = dno+tg(Ip+1)-1;

zn2 = dS2(In+1);
zn1 = dS1(In+1);
zn  = dS(In+1);

tn2 = dno+tg(In+1)-1;
tn1 = dno+tg(In+1)-1;
tn  = dno+tg(In+1)-1;

figure(1);clf;hold on;grid on;
    plot(tp2,cumsum(zp2),'r','LineWidth',4);
    plot(tn2,cumsum(zn2),'g','LineWidth',4);
    plot(tp1,cumsum(zp1),'r','LineWidth',2);
    plot(tn1,cumsum(zn1),'g','LineWidth',2);
    plot(tp,cumsum(zp),'r','LineWidth',1);
    plot(tn,cumsum(zn),'g','LineWidth',1);

    title(sprintf('\\DeltaS = \\DeltaDJI/DJI - \\Delta%s/%s',symbol,symbol));
    legend(...
        ['\Delta DJI/DJI after \DeltaS > ',num2str(T),'%'],['\Delta DJI/DJI after \DeltaS < -',num2str(T),'%'],...
        ['\Delta ',symbol,'/',symbol,' after \Delta S > ',num2str(T),'%'],['\Delta ',symbol,'/',symbol,' after \Delta S < -',num2str(T),'%'],...
        ['\DeltaS after \DeltaS > ',num2str(T),'%'],['\DeltaS after \DeltaS < -',num2str(T),'%'],...
        'Location','NorthWest');
    datetick;

break

S1d = 100*(S1-S1(1))/S1(1);
S2d = 100*(S2-S2(1))/S2(1);

figure(1);clf;
    plot(tg,S1d);hold on;grid on;
    plot(tg,S2d,'k');
    datetick;
    legend(symbol,' DJI');
figure(2);clf;
    plot(t1,S1d-S2d);hold on;grid on;
    datetick;
    legend(sprintf(' %s-DJI',symbol));

T = 2;
Nd = 10;
k = 1;

break
mean(dS(I+1))
mean(dS1(I+1))
mean(dS2(I+1))
break
while k < length(S1)
    S1d = 100*(S1(k:end)-S1(k))/S1(k);
    S2d = 100*(S2(k:end)-S2(k))/S2(k);
    d   = S1d-S2d;
    %a   = find(d > T,1);
    a   = find(d < -T,1);
    if (isempty(a))
        %k=k+b;
        k=k+1;
        continue;
    end;
    b   = a+Nd;

    if (k+b > length(S1d)),k=k+1;continue,end;
    r1 = 100*(S1(k+b)-S1(k+a))/S1(k+a);
    r2 = 100*(S2(k+b)-S2(k+a))/S2(k+a);
    d1(k) = r1-r2;
    R1(k) = r1;
    R2(k) = r2;
    T1(k) = t1(k+b-1);

    fprintf('%4d %4d %4d %4d %8.2f %8.2f %8.2f %8.2f %+9.2f %+9.2f %+9.2f\n',...
            k,T1(k),a,b,S1(k+a),S1(k+b),S2(k+a),S2(k+b),r1,r2,r1-r2);
    
    %k = k+b;

    if 0%(k == 3)
        figure(3);clf
            plot(S1d,'g','LineWidth',2);hold on;grid on;
            plot(S2d,'k','LineWidth',2);
            plot(d,'LineWidth',2);
            plot([a,a],[0,d(b)],'g-','MarkerSize',30);
            plot(a,d(a),'g.','MarkerSize',20);
            plot(b,d(b),'r.','MarkerSize',20);
            plot([b,b],[0,d(b)],'r-','MarkerSize',30);
            %axis([0,a+Nd+10,-d(a+Nd),d(a+Nd)]);
            legend(symbol,'DJI','\Delta','Location','NorthWest');
            keyboard
    end
    
    k = k+1;
end

S1d = S1/S1(1);
S2d = S2/S2(1);

if (~exist('T1')),break,end;
fprintf('---\n');
[T1u,I] = unique(T1,'first');
R1 = R1(I);
R2 = R2(I);
[T1s,Is] = sort(T1u);
R1 = R1(Is);
R2 = R2(Is);
T1s = T1s(2:end);
R1 = R1(2:end);
R2 = R2(2:end);

P1 = 1;
P2 = 1;
Pd = 1;
for i = 1:length(T1s)-1
    P1(i+1) = P1(i) + R1(i)*P1(i)/100;
    P2(i+1) = P2(i) + R2(i)*P2(i)/100;
    Pd(i+1) = Pd(i) + (R1(i)-R2(i))*Pd(i)/100;
end
figure(3);clf;hold on;grid on;
    %stairs(T1s,cumsum(R1(Is)),'g','LineWidth',3);
    %stairs(T1s,cumsum(R2(Is)),'b','LineWidth',3);
    stairs(T1s,P1,'g','LineWidth',3);
    stairs(T1s,P2,'b','LineWidth',3);
    stairs(T1s,Pd,'k','LineWidth',3);
    plot(t1,S1d,'g','LineWidth',1);
    plot(t2,S2d,'b','LineWidth',1);
    datetick
    legend(['\Delta',symbol],'\DeltaDJI','\Delta',symbol,'DJI','Location','NorthWest');
    %legend('\DeltaDJI',symbol,'DJI','Location','NorthWest');
    break    
k = 1;
while k < length(S1)
    S1d = 100*(S1(k:end)-S1(k))/S1(k);
    S2d = 100*(S2(k:end)-S2(k))/S2(k);
    d = S1d-S2d;
    a = find(d < T,1);
    if ~isempty(a)
        b = a+10;
        if (k+a+b > length(d)),break,end;

        bm = length(d(a:end));
        R1 = 100*(d(k+a+b)-d(k+a))/d(k+a);
        R2 = 100*(S2(k+a+b)-S2(k+a))/S2(k+a);
        if ~isempty(b)
            fprintf('%4d %4d %4d  %4d %+9.2f %+9.2f %+9.2f\n',...
                    k,k+a,k+b+a,b,R1,R2,R1-R2);
            P2(k) = R1-R2;
        else
            R = 100*(S2(end)-S2(k+a))/S2(k+a);
            fprintf('%4d %4d %4d+ %4d+ %.2f %+9.2f\n',k,k+a,k+bm+a,bm,0,-R);
            P2(k) = -R;
        end
    end
    %k = k+a+b;
    k = k+1;
end
figure(3);
    plot(cumsum(P2),'k');hold on;
