function [Symbol,Start] = stocks_info()

Symbol{1} = 'AAPL';
Start{1}  = [2015,3,19];

Symbol{2} = 'NKE';
Start{2}  = [2013,9,23];

Symbol{3} = 'GS';
Start{3}  = [2013,9,23];

Symbol{4} = 'MRK';
Start{4}  = [1979,6,29];

Symbol{5} = 'PG';
%Start{5}  = [1932,5,26];
Start{5}  = [1970,1,2]; % Earliest data from Yahoo Finance

Symbols{5} = 'TRV';
%Start{5}  = [1977,5,17]; 
Start{5}  = [1986,7,9]; % Name change

Symbol{6} = 'PFE';
Start{6} = [2004,4,8];

Symbol{7} = 'BA';
Start{7}  = [1987,5,12];

Symbols{8} = 'WMT';
Start{8}  = [1997,5,19];

Symbol{9} = 'JNJ';
Start{9}  = [1997,5,19];

Symbol{10} = 'GE';
%Starts{10}  = [1907,11,7];
Start{10}  = [1962,1,15];

Symbol{11} = 'MMM';
Start{11}  = [1976,8,9];

Symbol{12} = 'XOM';
Start{12}  = [1976,8,9];

Symbol{13} = 'AXP';
Start{13}  = [1982,8,30];

Symbol{14} = 'HD';
Start{14}  = [1999,11,1];

