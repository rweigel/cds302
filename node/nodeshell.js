var sys = require('sys')
var exec = require('child_process').exec;
var child;

// executes `pwd`
child = exec("ls -l", 
	function (error, stdout, stderr) {
  		console.log('stdout: ' + stdout);
  		console.log('stderr: ' + stderr);
  		if (error !== null) {
   			console.log('exec error: ' + error);
  		}
});
