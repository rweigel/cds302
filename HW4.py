# TODO: Create a version that passes array to struct.pack so that
# loop is not needed.
import time
import struct

start = time.clock()
f1 = open ('file1.bin','wb')
for x in range(1, 2 ** 16):
	f1.write(struct.pack('H',x ));
stop = time.clock()
print '{0:.5f},'.format(stop-start),
f1.close

start = time.clock()
f2 = open ('file2.bin','wb')
for x in range(1, 2 ** 16):
	f2.write(struct.pack('I',x ));
stop = time.clock()
print '{0:.5f},'.format(stop-start),
f2.close

start = time.clock()
f3 = open ('file3.bin','wb')
for x in range(1, 2 ** 16):
	f3.write(struct.pack('L',x ));
stop = time.clock()
print '{0:.5f},'.format(stop-start),
f3.close

start = time.clock()
f4 = open ('file4.bin','wb')
for x in range(1, 2 ** 16):
	f4.write(struct.pack('f',x ));
stop = time.clock()
print '{0:.5f},'.format(stop-start),
f4.close

start = time.clock()
f5 = open ('file5.bin','wb')
for x in range(1, 2 ** 16):
	f5.write(struct.pack('d',x ));
stop = time.clock()
print '{0:.5f}'.format(stop-start)
f5.close


