#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {

	struct timeval start, end;

	int i = 0;

	FILE *fpSUint;
	fpSUint = fopen("f1c.bin", "wb");

	FILE *fpUint;
	fpUint = fopen("f2c.bin", "wb");

	FILE *fpLUint;
	fpLUint = fopen("f3c.bin", "wb");

	FILE *fpFloat;
	fpFloat = fopen("f4c.bin", "wb");

	FILE *fpDouble;
	fpDouble = fopen("f5c.bin", "wb");

	short unsigned int *arraySUint;
	unsigned int *arrayUint;
	long unsigned int *arrayLUint;
	float *arrayFloat;
	double *arrayDouble;

	int N = pow(2,16);

	if ( NULL == (arraySUint = malloc(N * sizeof(short unsigned int))) ) {
	  printf("malloc failed\n");
	  return 1;
	}
	if ( NULL == (arrayUint = malloc(N * sizeof(unsigned int))) ) {
	  printf("malloc failed\n");
	  return 1;
	}
	if ( NULL == (arrayLUint = malloc(N * sizeof(long int))) ) {
	  printf("malloc failed\n");
	  return 1;
	}
	if ( NULL == (arrayFloat = malloc(N * sizeof(float))) ) {
	  printf("malloc failed\n");
	  return 1;
	}
	if ( NULL == (arrayDouble = malloc(N * sizeof(double))) ) {
	  printf("malloc failed\n");
	  return 1;
	}

	for (i=0;i<N;i++) {
	  arraySUint[i] = i;
	  arrayUint[i] = i;
	  arrayLUint[i] = i;
	  arrayFloat[i] = i;
	  arrayDouble[i] = i;
	}

	gettimeofday(&start, NULL);
	fwrite(arraySUint, sizeof(arraySUint[0]), N, fpSUint);
	gettimeofday(&end, NULL);
	printf("%f,", ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec))/1000000.);

	gettimeofday(&start, NULL);
	fwrite(arrayUint, sizeof(arrayUint[0]), N, fpUint);
	gettimeofday(&end, NULL);
	printf("%f,", ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec))/1000000.);

	gettimeofday(&start, NULL);
	fwrite(arrayLUint, sizeof(arrayLUint[0]), N, fpLUint);
	gettimeofday(&end, NULL);
	printf("%f,", ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec))/1000000.);

	gettimeofday(&start, NULL);
	fwrite(arrayFloat, sizeof(arrayFloat[0]), N, fpFloat);
	gettimeofday(&end, NULL);
	printf("%f,", ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec))/1000000.);

	gettimeofday(&start, NULL);
	fwrite(arrayDouble, sizeof(arrayDouble[0]), N, fpDouble);
	gettimeofday(&end, NULL);
	printf("%f\n", ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec))/1000000.);

	free(arraySUint);
	free(arrayLUint);
	free(arrayUint);
	free(arrayFloat);
	free(arrayDouble);

	return 0;
}
